const ContactForm = React.createClass({
  propTypes: {
    contact: React.PropTypes.object.isRequired
  },

  render: function() {
    return (
      <form className={"contactForm"}>
        <input
          type={"text"}
          name={"firstname"}
          placeholder={"Imię"}
          defaultValue={""}
        />
        <input
          type={"text"}
          name={"lastname"}
          placeholder={"Nazwisko"}
          defaultValue={""}
        />
        <input
          type={"email"}
          name={"email"}
          placeholder={"Email"}
          defaultValue={""}
        />
        <button type={"submit"}>'Dodaj kontakt'</button>
      </form>
    );
  }
});
